/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snowbear.impl;

import java.util.Arrays;
import ru.snowbear.Calculator;

/**
 * Реализация метода http://inf777.narod.ru/podgotovka_k_olympiad/zadachi_s_resh/island.htm Данное решение перенесено на
 * java
 *
 * @author m.padorin
 */
public class Method1 implements Calculator {

    private int[][] islandMap;
    private static final int MAX_LENGTH_OF_ARRAY = 5000;

    @Override
    public int calculate(int[][] islandMap) {
        if (islandMap.length == 0) {
            return 0;
        }
        if (islandMap.length > MAX_LENGTH_OF_ARRAY || islandMap[0].length > MAX_LENGTH_OF_ARRAY) {
            throw new ArrayIndexOutOfBoundsException("More then " + MAX_LENGTH_OF_ARRAY + ".");
        }
        //Окружим водой данный участок
        int[][] workMap = new int[islandMap.length + 2][islandMap[0].length + 2];
        for (int[] is : workMap) {
            Arrays.fill(is, 0);
        }
        for (int i = 0; i < islandMap.length; i++) {
            int[] is = islandMap[i];
            for (int j = 0; j < is.length - 1; j++) {
                workMap[i + 1][j + 1] = islandMap[i][j];
            }
        }

        this.islandMap = workMap;
        int count = 0;
        for (int i = 1; i < this.islandMap.length - 1; i++) {
            int[] is = this.islandMap[i];
            for (int j = 1; j < is.length - 1; j++) {
                if (this.islandMap[i][j] == 1) {
                    count++;
                    count(i, j);
                }
            }
        }
        return count;
    }

    /**
     * Процедура уничтожения островов
     *
     * @param i
     * @param j
     */
    public void count(int i, int j) {
        if (this.islandMap[i][j] != 1) {
            return;
        }
        this.islandMap[i][j] = 0;
        int l = j + 1;
        while (this.islandMap[i][l] == 1) {
            this.islandMap[i][l] = 0;
            count(i - 1, l);
            l++;
        }
        for (int k = j; k < l; k++) {
            count(i + 1, k);
        }
        count(i + 1, j);
        count(i - 1, j);
        count(i, j + 1);
        count(i, j - 1);

    }

    /**
     * Процедура уничтожения островов
     *
     * @param i
     * @param j
     */
    public void reqursiveCount(int i, int j) {
        if (this.islandMap[i][j] != 1) {
            return;
        }
        this.islandMap[i][j] = 0;
        reqursiveCount(i + 1, j);
        reqursiveCount(i - 1, j);
        reqursiveCount(i, j + 1);
        reqursiveCount(i, j - 1);
    }
}
