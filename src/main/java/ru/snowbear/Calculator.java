/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snowbear;

/**
 *
 * Интерфейс вычислителя остравов
 * @author m.padorin
 */
public interface Calculator {
    public int calculate(int[][] islandMap);
}
