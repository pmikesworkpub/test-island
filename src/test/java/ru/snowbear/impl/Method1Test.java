/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snowbear.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author m.padorin
 */
@RunWith(Parameterized.class)
public class Method1Test {

    @Parameterized.Parameter
    public int expResult = 0;
    @Parameterized.Parameter(1)
    public int[][] islandMap = null;
    private static final String CLASS_DATA_PATH = "ru/snowbear/impl";
    private static final String CHARSET = "UTF-8";
    private static final String POSTFIX_TEST_FILE = ".data";


    @Parameters(name = "{index}: expected {0}")
    public static Collection<Object[]>  loader() throws IOException {
        List<Object[]> result = new ArrayList();
        List<String> testFiles = IOUtils.readLines(Method1Test.class.getClassLoader().getResourceAsStream(CLASS_DATA_PATH), Charsets.toCharset(CHARSET));
        for (String testFile : testFiles) {
            if (testFile.endsWith(POSTFIX_TEST_FILE)) {
                String expectedResult = testFile.substring(0, testFile.indexOf("."));
                int[][] data = readFromFile(testFile);
                result.add(new Object[]{Integer.parseInt(expectedResult), data});
            }
        }
        return result;
    }

    private static int[][] readFromFile(String testFile) throws IOException {
        List<int[]> lines = new ArrayList<>();
        try (InputStream is = Method1Test.class.getClassLoader().getResourceAsStream(CLASS_DATA_PATH + "/" +testFile)){
            Scanner scanner = new Scanner(is);    
            while (scanner.hasNextLine()) {
                String lineStr = scanner.nextLine();
                int[] line = new int[lineStr.length()];
                for (int i = 0; i < line.length; i++) {// Потенциально дырка , можем выйти из lineScanner
                    line[i] = Character.getNumericValue(lineStr.charAt(i));
                }
                lines.add(line);
            }
        }
        if (lines.isEmpty()) {
            return new int[][]{};
        } else {
            int[][] result = new int[lines.size()][lines.get(0).length];
            for (int i = 0; i < lines.size(); i++) {
                result[i] = lines.get(i);
            }
            return result;
        }
    }
    
 
    /**
     * Test of calculate method, of class Method1.
     */
    @Test
    public void testCalculate() {
        System.out.println("calculate");
        Method1 instance = new Method1();
        int result = instance.calculate(islandMap);
        assertEquals(expResult, result);
    }

}
